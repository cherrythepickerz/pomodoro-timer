import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";
//@ts-ignore
console.log = Reactotron.log;

//@ts-ignore
Reactotron.configure() // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from // controls connection & communication settings
  .use(reactotronRedux())
  .useReactNative() // add all built-in react native plugins
  .connect(); // let's connect!

export default Reactotron;

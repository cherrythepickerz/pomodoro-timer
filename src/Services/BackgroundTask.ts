import { store } from "src/Redux/createStore";
import { setTimer } from "src/Redux/Reducers/TimerReducer";

export default () => {
  const state = store.getState();

  setInterval(() => {
    const timer = state.timer.timer;
    store.dispatch(setTimer(timer - 1));
  }, 1000)
}
import { Platform } from "react-native";
import moment from "moment";
import { Notifications } from "react-native-notifications";
import { NotificationsIOS } from "react-native-notifications/lib/dist/NotificationsIOS";
import {
  checkNotifications,
  requestNotifications,
} from "react-native-permissions";

export type NotificationConfigType = {
  body: string;
  title: string;
  sound?: string;
  silent: boolean;
  category?: string;
  userInfo?: string;
  date?: string;
};

class NotificationsService {
  constructor() {
    Notifications.events().registerNotificationReceivedForeground(
      (notification, completion) => {
        console.log("Notification Received - Foreground", notification.payload);

        // Calling completion on iOS with `alert: true` will present the native iOS inApp notification.
        completion({ alert: true, sound: true, badge: false });
      }
    );

    Notifications.events().registerNotificationOpened(
      (response, completion) => {
        // console.log(
        //   "Notification opened by device user",
        // );
        // console.log(
        //   //@ts-ignore
        //   `Notification opened with an action identifier: ${response.action.identifier} and response text: ${response.action.text}`,
        // );
        // completion();
      }
    );

    Notifications.events().registerNotificationReceivedBackground(
      (notification, completion) => {
        console.log("Notification Received - Background", notification.payload);

        // Calling completion on iOS with `alert: true` will present the native iOS inApp notification.
        completion({ alert: true, sound: true, badge: true });
      }
    );
  }

  registerRemoteNotifications = async () => {
    // tslint:disable-next-line: await-promise

    Notifications.ios.registerRemoteNotifications();

    Notifications.events().registerRemoteNotificationsRegistered((event) => {});
    Notifications.events().registerRemoteNotificationsRegistrationFailed(
      (event) => {}
    );
  };

  checkPermissionsIos = async () => {
    requestNotifications(["alert", "sound", "badge"]).then(
      ({ status, settings }) => {
        // …
      }
    );
  };

  cancellAllNotificationsIos = () => {
    Notifications.ios.cancelAllLocalNotifications();
  };

  sendNotification = (config) => {
    // tslint:disable-next-line:variable-name
    const _config = {
      ...config,
      sound: "chime.aiff",
      type: "",
      thread: "",
      payload: "",
      title: "123123",
    };

    if (Platform.OS === "android") {
      this.sendAndroid(_config);
    } else {
      this.sendIos(_config);
    }
  };

  sendIos = (config: NotificationConfigType) => {
    //@ts-ignore
    Notifications.postLocalNotification(config, moment().unix());
  };

  sendAndroid = (config) => {
    Notifications.postLocalNotification(
      //@ts-ignore
      config,
      2
    );
  };
}

export default new NotificationsService();

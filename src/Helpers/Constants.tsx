export const POMODORO_CICLES = [
  "work",
  "break",
  // "work",
  // "break",
  // "work",
  // "break",
  // "work",
  // "break",
  // "work",
  "longBreak",
];

export const POMODORO_MODES = {
  longBreak: { id: "longBreak", timer: 0.1 * 60, title: "Long break" },
  work: { id: "work", timer: 0.2 * 60, title: "Work" },
  break: { id: "break", timer: 0.1 * 60, title: "Break" },
};

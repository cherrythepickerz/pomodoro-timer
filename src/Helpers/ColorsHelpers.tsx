import { POMODORO_MODES } from "./Constants";
import workIcon from "src/Images/workspace.png";
import shortBreakIcon from "src/Images/breakfast.png";
import longBreakIcon from "src/Images/mountain.png";

export const setColor = (mode) => {
  switch (mode) {
    case POMODORO_MODES.work.id:
      return {
        primaryColor: "#FF1654",
        secondaryColor: "#ff576e",
        icon: workIcon,
      };
    case POMODORO_MODES.break.id:
      return {
        primaryColor: "#247BA0",
        secondaryColor: "#5490af",
        icon: shortBreakIcon,
      };
    case POMODORO_MODES.longBreak.id:
      return {
        primaryColor: "#70C1B3",
        secondaryColor: "#8acbbf",
        icon: longBreakIcon,
      };

    default:
      return {
        primaryColor: "#70C1B3",
        secondaryColor: "#8acbbf",
      };
  }
};

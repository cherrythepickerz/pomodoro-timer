import * as React from "react";
import { Platform, View } from "react-native";
import {
  NotificationResponse,
  Notifications,
} from "react-native-notifications";

//tslint:disable

class PushNotificationManager extends React.PureComponent {
  public componentDidMount = async () => {
    // return;
    // this.registerDevice();
    // this.registerNotificationEvents();
  };

  public registerDevice = () => {
    Notifications.events().registerRemoteNotificationsRegistered((event) => {
      // TODO: Send the token to my server so it could send back push notifications...
      console.log("Device Token Received", event.deviceToken);
    });
    Notifications.events().registerRemoteNotificationsRegistrationFailed(
      (event) => {
        console.error(event);
      }
    );

    // Notifications.registerRemoteNotifications();
  };

  public registerNotificationEvents = () => {
    Notifications.events().registerNotificationReceivedForeground(
      (notification, completion) => {
        console.log("Notification Received - Foreground", notification);
        // Calling completion on iOS with `alert: true` will present the native iOS inApp notification.
        completion({ alert: false, sound: false, badge: false });
      }
    );

    Notifications.events().registerNotificationOpened(
      (notification: NotificationResponse, completion) => {
        console.log("Notification opened by device user", notification);
        console.log(
          `Notification opened with an action identifier: ${
            notification.identifier
          }`
        );

        completion();
      }
    );

    Notifications.events().registerNotificationReceivedBackground(
      (notification, completion) => {
        console.log("Notification Received - Background", notification);

        // Calling completion on iOS with `alert: true` will present the native iOS inApp notification.
        completion({ alert: true, sound: true, badge: false });
      }
    );
  };

  public render() {
    const { children } = this.props;
    return <>{children}</>;
  }
}

export default PushNotificationManager;

import React, { PureComponent } from "react";
import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
  Vibration,
  Image,
  SafeAreaView,
} from "react-native";
import AnimatedLayout from "./Layout";
import AnimatedTimer from "./AnimatedTimer";
import { POMODORO_MODES, POMODORO_CICLES } from "../../Helpers/Constants";
import CustomTimerFormModal from "../CustomTimerModalForm";
import { connect } from "react-redux";
import {
  setTimer,
  TimerReduxType,
  setCycle,
  changeMode,
  setTimerPaused,
  setTimerStarted,
  resetTimer,
} from "src/Redux/Reducers/TimerReducer";
import moment from "moment";
import NotificationService from "src/Services/NotificationService";
import { Notification } from "react-native-notifications";

interface IProps {
  cycle: number;
  timer: number;
  setTimer: (number: number) => void;
  setCycle: (number: number) => void;
  setTimerPaused: () => void;
  setTimerStarted: () => void;
  isTimerStarted: boolean;
  mode: any;
}

class Timer extends PureComponent<IProps> {
  interval;

  state = {
    isTimerStarted: false,
    cicle: 0,
    pomodoroModes: POMODORO_MODES,
    defaultTimer: POMODORO_MODES.work.timer,
    mode: POMODORO_MODES.work.id,
    isModalVisible: false,
  };

  componentDidMount = () => {
    NotificationService.checkPermissionsIos();
    this.props.setTimer(POMODORO_MODES.work.timer);
  };

  onCustomTimerSubmit = (customModes) => {
    this.setState({
      pomodoroModes: customModes,
      isTimerStarted: false,
      cicle: 0,
      defaultTimer: customModes.work.timer,
      timer: customModes.work.timer,
      mode: customModes.work.id,
    });
  };

  pauseTimer = () => {
    this.props.setTimerPaused();
    NotificationService.cancellAllNotificationsIos();
    this.interval = clearInterval(this.interval);
  };

  componentDidUpdate = (prevProps: IProps) => {
    if (prevProps.isTimerStarted && !this.props.isTimerStarted) {
      clearInterval(this.interval);
    }
  };

  startTimer = () => {
    const { pomodoroModes } = this.state;
    if (this.props.isTimerStarted) {
      return;
    }

    this.props.setTimerStarted();
    const d1 = moment()
      .add(this.props.timer, "seconds")
      .toISOString();
    const notificationConfig = {
      body: "Pomodoro timer",
      title: pomodoroModes[POMODORO_CICLES[this.props.cycle]].id,
      fireDate: d1,
      silent: false,
    };

    NotificationService.sendIos(notificationConfig);

    this.interval = setInterval(() => {
      if (this.props.timer <= 1) {
        this.changeMode();
        return;
      }

      this.props.setTimer(this.props.timer - 1);
    }, 1000);
  };

  resetTimer = () => {
    NotificationService.cancellAllNotificationsIos();
    this.props.resetTimer();

    clearInterval(this.interval);
  };

  changeMode = () => {
    const { pomodoroModes } = this.state;

    const cycle =
      this.props.cycle + 1 === POMODORO_CICLES.length
        ? 0
        : this.props.cycle + 1;

    console.log("CYCLE", cycle);

    this.props.changeMode({
      cycle: cycle,
      isTimerStarted: false,
      mode: pomodoroModes[POMODORO_CICLES[cycle]].id,
      defaultTimer: pomodoroModes[POMODORO_CICLES[cycle]].timer,
      timer: pomodoroModes[POMODORO_CICLES[cycle]].timer,
    });

    clearInterval(this.interval);
  };

  handleModalClose = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  render() {
    if (!this.props.timer)
      return (
        <View
          style={{ height: "100%", width: "100%", backgroundColor: "blue" }}
        />
      );

    return (
      <AnimatedLayout
        isTimerStarted={this.props.isTimerStarted}
        mode={this.props.mode}
      >
        <AnimatedTimer
          onPress={
            this.state.isTimerStarted ? this.pauseTimer : this.startTimer
          }
          isTimerStarted={this.props.isTimerStarted}
          cicle={this.props.cycle}
          timer={this.props.timer}
          mode={this.props.mode}
        />
        <View style={styles.btnContainer}>
          <Button
            color={this.props.isTimerStarted ? "#ececec" : "#FF1654"}
            disabled={this.props.isTimerStarted}
            onPress={this.startTimer}
            title="Start"
          />
          <Button
            disabled={!this.props.isTimerStarted}
            color={"#FF1654"}
            onPress={this.pauseTimer}
            title="Pause"
          />
          <Button color={"#FF1654"} onPress={this.resetTimer} title="Reset" />
        </View>
        {/* <View
          style={{ flex: 1, alignItems: "center", justifyContent: "flex-end" }}
        >
          <Text style={styles.txt}>Whant to set your custom timer?</Text>
          <TouchableOpacity
            style={{ marginTop: 8 }}
            onPress={() => {
              this.setState({ isModalVisible: true });
            }}
          >
            <Text style={[styles.txt, { textDecorationLine: "underline" }]}>
              Press here
            </Text>
          </TouchableOpacity>
        </View>
        <CustomTimerFormModal
          handleClose={this.handleModalClose}
          visible={this.state.isModalVisible}
          customTimerSubmit={this.onCustomTimerSubmit}
        /> */}
      </AnimatedLayout>
    );
  }
}

const mapStateToProps = ({ timer }: { timer: TimerReduxType }) => ({
  timer: timer.timer,
  cycle: timer.cycle,
  isTimerStarted: timer.isTimerStarted,
  mode: timer.mode,
});

const mapActionsToProps = {
  setTimer,
  setCycle,
  changeMode,
  setTimerPaused,
  setTimerStarted,
  resetTimer,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Timer);

const styles = StyleSheet.create({
  txt: {
    color: "#F3FFBD",
  },
  btnContainer: {
    // width: "60%",
    justifyContent: "space-around",
    flexDirection: "row",
  },
  btn: {
    color: "#F3FFBD",
  },
});

import React, { PureComponent } from "react";
import {
  Animated,
  SafeAreaView,
  View,
  Image,
  Text,
  StyleSheet,
} from "react-native";
import { setColor } from "../../Helpers/ColorsHelpers";

export default class Layout extends PureComponent {
  state = {
    primaryColor: undefined,
  };

  componentDidMount = () => {
    this.setBackgroundColors();
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.mode !== prevProps.mode) {
      this.setBackgroundColors();
    }
  };

  setBackgroundColors = () => {
    const data = setColor(this.props.mode);

    this.setState({
      primaryColor: data.primaryColor,
      secondaryColor: data.secondaryColor,
      icon: data.icon,
    });
  };

  render() {
    const { secondaryColor, icon, primaryColor } = this.state;

    return (
      <SafeAreaView
        style={{
          height: "100%",
          width: "100%",
          backgroundColor: secondaryColor,
        }}
      >
        <Animated.View
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
            backgroundColor: "#faf8f7",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              height: "30%",
              width: "100%",
              backgroundColor: secondaryColor,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              style={{ height: 150, alignSelf: "flex-start" }}
              resizeMode={"contain"}
              source={icon}
            />
            <View
              style={[styles.infoContainer, { backgroundColor: primaryColor }]}
            >
              <View style={{ flex: 1 }}>
                <Text style={styles.title}>52</Text>
                <Text style={styles.subtitle}>working sessions</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.title}>52</Text>
                <Text style={styles.subtitle}>working sessions</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.title}>52</Text>
                <Text style={styles.subtitle}>working sessions</Text>
              </View>
            </View>
          </View>
          {this.props.children}
        </Animated.View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    color: "#F3FFBD",
  },
  subtitle: {
    fontSize: 14,
    color: "#FFF",
  },
  infoContainer: {
    height: "30%",
    width: "100%",
    position: "absolute",
    opacity: 0.6,
    zIndex: 9999,
    flexDirection: "row",
    bottom: 0,
    padding: 16,
  },
});

import React from "react";
import { AppState } from "react-native";
import { useStore } from "react-redux";
import {
  setTimer,
  setBackgroundTimestamp,
  changeMode,
  setTimerPaused,
} from "src/Redux/Reducers/TimerReducer";
//tslint:disable
//@ts-ignore
import BackgroundTimer from "react-native-background-timer";
import NotificationService from "src/Services/NotificationService";
import moment from "moment";
import { POMODORO_MODES, POMODORO_CICLES } from "src/Helpers/Constants";

export default (props: any) => {
  const store = useStore();
  React.useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange);
    return () => {
      AppState.removeEventListener("change", handleAppStateChange);
    };
  }, []);

  const handleAppStateChange = (state: AppState["currentState"]) => {
    const reduxState = store.getState();
    const isTimerStarted = reduxState.timer.isTimerStarted;
    if (!isTimerStarted) {
      return;
    }

    switch (state) {
      case "active":
        const cycle = reduxState.timer.cycle;
        const timerValue = reduxState.timer.timer;
        const backgroundTimestamp = reduxState.timer.backgroundTimestamp;
        const diff = moment(moment().unix()).diff(backgroundTimestamp);
        if (timerValue - diff <= 0) {
          console.log(cycle, timerValue);
          store.dispatch(
            changeMode({
              cycle: cycle === POMODORO_CICLES.length ? 0 : cycle + 1,
              isTimerStarted: false,
              mode: POMODORO_MODES[POMODORO_CICLES[cycle + 1]].id,
              defaultTimer: POMODORO_MODES[POMODORO_CICLES[cycle + 1]].timer,
              timer: POMODORO_MODES[POMODORO_CICLES[cycle + 1]].timer,
            })
          );
          store.dispatch(setBackgroundTimestamp(undefined));
          store.dispatch(setTimerPaused());
          break;
        }
        store.dispatch(setTimer(timerValue - diff));
        store.dispatch(setBackgroundTimestamp(undefined));
        break;
      case "background":
        const timer = reduxState.timer.timer;
        store.dispatch(setBackgroundTimestamp(moment().unix()));
        store.dispatch(setTimer(timer));

        console.log("Configured local notification");

      default:
        return;
    }
  };

  return <>{props.children}</>;
};

import { applyMiddleware, compose, createStore } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import reduxThunk from "redux-thunk";
import ReactotronConfig from "src/Config/ReactotronConfig";
import RootReducer from "src/Redux/RootReducer";
import AsyncStorage from "@react-native-community/async-storage";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: [""],
};

const persistedReducer = persistReducer(persistConfig, RootReducer);

const store = createStore(
  persistedReducer,
  compose(
    applyMiddleware(reduxThunk),
    //@ts-ignore
    // tslint:disable-next-line: no-unsafe-any
    ReactotronConfig.createEnhancer()
  )
);

const persistor = persistStore(store);

export { store, persistor };

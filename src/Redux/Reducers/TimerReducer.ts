import { POMODORO_MODES } from "src/Helpers/Constants";

const INCREMENT_TIMER = "INCREMENT_TIMER";
const SET_TIMER = "SET_TIMER";
const SET_CYCLE = "SET_CYCLE";
const SET_BACKGROUND_TIMESTAMP = "SET_BACKGROUND_TIMESTAMP";
const SET_TIMER_STARTED = "SET_TIMER_STARTED";
const SET_TIMER_PAUSED = "SET_TIMER_PAUSED";
const RESET_TIMER = "RESET_TIMER";
const CHANGE_MODE = "CHANGE_MODE";

const defaultState = {
  timer: undefined,
  isTimerStarted: false,
  cycle: 0,
  pomodoroModes: POMODORO_MODES,
  defaultTimer: POMODORO_MODES.work.timer,
  mode: POMODORO_MODES.work.id,
  backgroundTimestamp: undefined,
};

export type TimerReduxType = {
  timer: number;
  backgroundTimestamp?: number;
  isTimerStarted: boolean;
  cycle: number;
};

const reducer = (state = defaultState, action: any) => {
  switch (action.type) {
    case INCREMENT_TIMER:
      return {
        ...state,
      };
    case SET_CYCLE: {
      return {
        ...state,
        cycle: action.payload,
      };
    }
    case SET_BACKGROUND_TIMESTAMP:
      return {
        ...state,
        backgroundTimestamp: action.payload,
      };
    case SET_TIMER:
      return {
        ...state,
        timer: action.payload,
      };
    case SET_TIMER_STARTED:
      return {
        ...state,
        isTimerStarted: true,
      };
    case SET_TIMER_PAUSED: {
      return {
        ...state,
        isTimerStarted: false,
      };
    }
    case CHANGE_MODE: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case RESET_TIMER: {
      const { defaultTimer } = state;
      return {
        ...state,
        timer: defaultTimer,
        isTimerStarted: false,
      };
    }
    default:
      return state;
  }
};

export const setTimer = (value: number) => {
  return {
    type: SET_TIMER,
    payload: value,
  };
};

export const incrementTimer = () => {
  return {
    type: INCREMENT_TIMER,
  };
};

export const setCycle = (cycle: number) => {
  return {
    type: SET_CYCLE,
    payload: cycle,
  };
};

export const setBackgroundTimestamp = (timestamp?: number) => {
  return {
    type: SET_BACKGROUND_TIMESTAMP,
    payload: timestamp,
  };
};

export const setTimerStarted = () => {
  return {
    type: SET_TIMER_STARTED,
    payload: true,
  };
};

export const setTimerPaused = () => {
  return {
    type: SET_TIMER_PAUSED,
    payload: false,
  };
};

export const resetTimer = () => {
  return {
    type: RESET_TIMER,
  };
};

export const changeMode = ({
  cycle,
  isTimerStarted,
  mode,
  timer,
  defaultTimer,
}: any) => {
  return {
    type: CHANGE_MODE,
    payload: { cycle, isTimerStarted, mode, timer, defaultTimer },
  };
};

export default reducer;
//tslint:disable

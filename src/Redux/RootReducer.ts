import { combineReducers } from "redux";
import TimerReducer from './Reducers/TimerReducer';

const appReducer = combineReducers({
  timer: TimerReducer
});

export default appReducer;

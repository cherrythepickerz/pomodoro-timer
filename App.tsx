import React from "react";
import { StyleSheet, Vibration, View } from "react-native";
import { Provider } from "react-redux";
import Timer from "./src/Features/Timer";
import PushNotificationManager from "./src/Features/PushNotificationManager";
import BackgroundManager from "./src/Features/BackgroundManager";
import { store } from "./src/Redux/createStore";
import "./src/Config/ReactotronConfig";

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Provider store={store}>
          <BackgroundManager>
            <PushNotificationManager>
              <Timer />
            </PushNotificationManager>
          </BackgroundManager>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    width: "100%",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
